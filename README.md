This is a repository of all survey files made available 
for research or clinical purposes by Neill Watson, PhD.

Survey files are in lss format, a survey structure for 
LimeSurvey.  For more on lss, see 
http://manual.limesurvey.org/wiki/Exporting_a_survey_structure#LSS_export

Surveys are currently untested!
