��          ,      <       P   k   Q   &  �   ^   �                     One or more mandatory questions have not been answered. You cannot proceed until these have been completed. Project-Id-Version: LimeSurvey language file
Report-Msgid-Bugs-To: http://translate.limesurvey.org/
POT-Creation-Date: 2013-07-31 22:05:41+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2013-09-10 11:54-0500
Last-Translator: Jack Warner <jackwarner@wmalumni.com>
Language-Team: LimeSurvey Team <c_schmitz@users.sourceforge.net>
X-Poedit-KeywordsList: gT;ngT:1,2;eT;neT:1,2
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: ..\..\..\
X-Generator: Poedit 1.5.7
X-Poedit-SearchPath-0: .
 One or more questions have not been answered. Please answer the question(s) and then continue. 